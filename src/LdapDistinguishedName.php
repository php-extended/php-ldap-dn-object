<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-dn-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

/**
 * LdapDistinguishedName class file.
 * 
 * This class is a simple implementation of thee LdapDistinguishedNameInterface.
 * 
 * @author Anastaszor
 */
class LdapDistinguishedName implements LdapDistinguishedNameInterface
{
	
	/**
	 * The parts of the dn.
	 * 
	 * @var array<LdapDistinguishedPartInterface>
	 */
	protected array $_parts = [];
	
	/**
	 * Builds a new LdapDistinguishedName with the given parts (parts are 
	 * array values with field name as key and field value as value).
	 * 
	 * @param array<string, string>|array<array<int, string>>|array<array<string, string>>|array<LdapDistinguishedPartInterface> $parts
	 */
	public function __construct(array $parts = [])
	{
		foreach($parts as $key => $part)
		{
			if($part instanceof LdapDistinguishedPartInterface)
			{
				$this->_parts[] = $part;
				continue;
			}
			
			// [["cn", "foo"], ["ou", "bar"], ["c", "uk"]]
			if(\is_array($part) && isset($part[0], $part[1]))
			{
				$this->_parts[] = new LdapDistinguishedPart((string) $part[0], (string) $part[1]);
				continue;
			}
			
			// [["cn" => "foo"], ["ou" => "bar"], ["c" => "uk"]]
			if(\is_array($part))
			{
				foreach($part as $key2 => $value)
				{
					$this->_parts[] = new LdapDistinguishedPart((string) $key2, (string) $value);
				}
				continue;
			}
			
			$this->_parts[] = new LdapDistinguishedPart((string) $key, (string) $part);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->getStringRepresentation();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapDistinguishedNameInterface::isEmpty()
	 */
	public function isEmpty() : bool
	{
		return 0 === \count($this->_parts);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapDistinguishedNameInterface::getDepths()
	 */
	public function getDepths() : int
	{
		return \count($this->_parts);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapDistinguishedNameInterface::getParentDistinguishedName()
	 */
	public function getParentDistinguishedName() : LdapDistinguishedNameInterface
	{
		return new self(\array_slice($this->_parts, 0, -1));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapDistinguishedNameInterface::getStringRepresentation()
	 */
	public function getStringRepresentation() : string
	{
		$parts = [];
		
		foreach(\array_reverse($this->_parts) as $part)
		{
			/** @var LdapDistinguishedPartInterface $part */
			$parts[] = $part->getStringRepresentation();
		}
		
		return \implode(',', $parts);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapDistinguishedNameInterface::getFinalIdentifierField()
	 */
	public function getFinalIdentifierField() : string
	{
		$last = \end($this->_parts);
		if(false === $last)
		{
			return '';
		}
		
		return $last->getName();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapDistinguishedNameInterface::getFinalIdentifierValue()
	 */
	public function getFinalIdentifierValue() : string
	{
		$last = \end($this->_parts);
		if(false === $last)
		{
			return '';
		}
		
		return $last->getValue();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapDistinguishedNameInterface::append()
	 */
	public function append(string $field, string $value) : LdapDistinguishedNameInterface
	{
		return $this->appendPart(new LdapDistinguishedPart($field, $value));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapDistinguishedNameInterface::appendPart()
	 */
	public function appendPart(LdapDistinguishedPartInterface $part) : LdapDistinguishedNameInterface
	{
		$newParts = $this->_parts;
		$newParts[] = $part;
		
		return new self($newParts);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapDistinguishedNameInterface::contains()
	 */
	public function contains(LdapDistinguishedNameInterface $other) : bool
	{
		$count = 0;
		
		foreach($other as $otherLdPart)
		{
			/** @var LdapDistinguishedPartInterface $otherLdPart */
			if(!isset($this->_parts[$count]))
			{
				return false;
			}
			
			/** @var LdapDistinguishedPartInterface $part */
			$part = $this->_parts[$count];
			
			if($part->getName() !== $otherLdPart->getName())
			{
				return false;
			}
			
			if($part->getValue() !== $otherLdPart->getValue())
			{
				return false;
			}
			
			$count++;
		}
		
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapDistinguishedNameInterface::equals()
	 */
	public function equals($other) : bool
	{
		return $other instanceof LdapDistinguishedNameInterface
			&& $this->contains($other)
			&& $other->contains($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapDistinguishedNameInterface::intersect()
	 */
	public function intersect(LdapDistinguishedNameInterface $other) : LdapDistinguishedNameInterface
	{
		$count = 0;
		$parts = [];
		
		foreach($other as $otherLdPart)
		{
			/** @var LdapDistinguishedPartInterface $otherLdPart */
			if(!isset($this->_parts[$count]))
			{
				return $this;
			}
			
			/** @var LdapDistinguishedPartInterface $part */
			$part = $this->_parts[$count];
			
			if($part->getName() !== $otherLdPart->getName())
			{
				return new self($parts);
			}
			
			if($part->getValue() !== $otherLdPart->getValue())
			{
				return new self($parts);
			}
			
			$count++;
			$parts[] = $otherLdPart;
		}
		
		return $other;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapDistinguishedNameInterface::truncate()
	 */
	public function truncate(int $level) : LdapDistinguishedNameInterface
	{
		if(0 > $level)
		{
			$level = 0;
		}
		
		return new self(\array_slice($this->_parts, 0, $level, true));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::rewind()
	 * @psalm-suppress InvalidFalsableReturnType
	 */
	public function current() : LdapDistinguishedPartInterface
	{
		/** @phpstan-ignore-next-line */ /** @psalm-suppress FalsableReturnStatement */
		return \current($this->_parts);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::next()
	 */
	public function next() : void
	{
		\next($this->_parts);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::key()
	 */
	public function key() : int
	{
		return (int) \key($this->_parts);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::valid()
	 */
	public function valid() : bool
	{
		return false !== \current($this->_parts);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Iterator::rewind()
	 */
	public function rewind() : void
	{
		\reset($this->_parts);
	}
	
}
