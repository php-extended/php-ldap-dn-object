<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-dn-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ldap\LdapDistinguishedName;
use PhpExtended\Ldap\LdapDistinguishedPart;
use PhpExtended\Ldap\LdapDistinguishedPartInterface;
use PHPUnit\Framework\TestCase;

/**
 * LdapDistinguishedNameTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ldap\LdapDistinguishedName
 *
 * @internal
 *
 * @small
 */
class LdapDistinguishedNameTest extends TestCase
{
	
	/**
	 * @var LdapDistinguishedName
	 */
	protected LdapDistinguishedName $_ldn;
	
	public function testIsEmpty() : void
	{
		$this->assertFalse($this->_ldn->isEmpty());
	}
	
	public function testIsEmpty2() : void
	{
		$this->assertTrue((new LdapDistinguishedName())->isEmpty());
	}
	
	public function testDepths() : void
	{
		$this->assertEquals(4, $this->_ldn->getDepths());
	}
	
	public function testEmptyParent() : void
	{
		$expected = new LdapDistinguishedName();
		$this->assertEquals('', $expected->getParentDistinguishedName()->__toString());
	}
	
	public function testParent() : void
	{
		$this->assertEquals('ou=bar,o=baz,c=fr', $this->_ldn->getParentDistinguishedName()->__toString());
	}
	
	public function testToString() : void
	{
		$this->assertEquals('cn=foo,ou=bar,o=baz,c=fr', $this->_ldn->__toString());
	}
	
	public function testFinalIdentifier() : void
	{
		$this->assertEquals('cn', $this->_ldn->getFinalIdentifierField());
	}
	
	public function testFinalValue() : void
	{
		$this->assertEquals('foo', $this->_ldn->getFinalIdentifierValue());
	}
	
	public function testAppend() : void
	{
		$expected = new LdapDistinguishedName(['c' => 'fr', 'o' => 'baz', 'ou' => 'bar', 'cn' => 'foo', 'uid' => 'foobar']);
		$append = $this->_ldn->append('uid', 'foobar');
		$this->assertEquals($expected, $append);
		$this->assertEquals('uid=foobar,cn=foo,ou=bar,o=baz,c=fr', $append->__toString());
	}
	
	public function testContainsEmpty() : void
	{
		$this->assertTrue($this->_ldn->contains(new LdapDistinguishedName()));
	}
	
	public function testContainsReal() : void
	{
		$this->assertTrue($this->_ldn->contains(new LdapDistinguishedName(['c' => 'fr', 'o' => 'baz', 'ou' => 'bar'])));
	}
	
	public function testContainsItself() : void
	{
		$this->assertTrue($this->_ldn->contains($this->_ldn));
	}
	
	public function testDoesNotContainValue() : void
	{
		$this->assertFalse($this->_ldn->contains(new LdapDistinguishedName(['c' => 'fr', 'o' => 'baz', 'ou' => 'bar', 'cn' => 'foobar'])));
	}
	
	public function testDoesNotContainField() : void
	{
		$this->assertFalse($this->_ldn->contains(new LdapDistinguishedName(['c' => 'fr', 'o' => 'baz', 'ou' => 'bar', 'uid' => 'foo'])));
	}
	
	public function testDoesNotContainMore() : void
	{
		$this->assertFalse($this->_ldn->contains(new LdapDistinguishedName(['c' => 'fr', 'o' => 'baz', 'ou' => 'bar', 'cn' => 'foo', 'uid' => 'foobar'])));
	}
	
	public function testEqualsThing() : void
	{
		$this->assertFalse($this->_ldn->equals(null));
	}
	
	public function testEqualsSame() : void
	{
		$this->assertTrue($this->_ldn->equals($this->_ldn));
	}
	
	public function testEqualsLess() : void
	{
		$this->assertFalse($this->_ldn->equals(new LdapDistinguishedName()));
	}
	
	public function testEqualsDiff() : void
	{
		$this->assertFalse($this->_ldn->equals(new LdapDistinguishedName(['c' => 'fr', 'o' => 'baz', 'ou' => 'bar', 'cn' => 'foobar'])));
	}
	
	public function testEqualsKey() : void
	{
		$this->assertFalse($this->_ldn->equals(new LdapDistinguishedName(['c' => 'fr', 'o' => 'baz', 'ou' => 'bar', 'uid' => 'foo'])));
	}
	
	public function testIntersectEmpty() : void
	{
		$this->assertEquals(new LdapDistinguishedName(), $this->_ldn->intersect(new LdapDistinguishedName()));
	}
	
	public function testIntersectItself() : void
	{
		$this->assertEquals($this->_ldn, $this->_ldn->intersect($this->_ldn));
	}
	
	public function testIntersectLess() : void
	{
		$other = new LdapDistinguishedName(['c' => 'fr', 'o' => 'baz', 'ou' => 'bar']);
		$this->assertEquals($other, $this->_ldn->intersect($other));
		$this->assertEquals($other, $other->intersect($this->_ldn));
	}
	
	public function testIntersectValue() : void
	{
		$other = new LdapDistinguishedName(['c' => 'fr', 'o' => 'baz', 'ou' => 'bar', 'cn' => 'foobar']);
		$res = new LdapDistinguishedName(['c' => 'fr', 'o' => 'baz', 'ou' => 'bar']);
		$this->assertEquals($res, $this->_ldn->intersect($other));
		$this->assertEquals($res, $other->intersect($this->_ldn));
	}
	
	public function testIntersectField() : void
	{
		$other = new LdapDistinguishedName(['c' => 'fr', 'o' => 'baz', 'ou' => 'bar', 'uid' => 'foo']);
		$res = new LdapDistinguishedName(['c' => 'fr', 'o' => 'baz', 'ou' => 'bar']);
		$this->assertEquals($res, $this->_ldn->intersect($other));
		$this->assertEquals($res, $other->intersect($this->_ldn));
	}
	
	public function testIntersectMore() : void
	{
		$other = new LdapDistinguishedName(['c' => 'fr', 'o' => 'baz', 'ou' => 'bar', 'cn' => 'foo', 'uid' => 'foobar']);
		$this->assertEquals($this->_ldn, $this->_ldn->intersect($other));
		$this->assertEquals($this->_ldn, $other->intersect($this->_ldn));
	}
	
	public function testTruncateToNegative() : void
	{
		$this->assertEquals(new LdapDistinguishedName(), $this->_ldn->truncate(-1));
	}
	
	public function testTruncateToZero() : void
	{
		$this->assertEquals(new LdapDistinguishedName(), $this->_ldn->truncate(0));
	}
	
	public function testTruncateReal() : void
	{
		$this->assertEquals(new LdapDistinguishedName(['c' => 'fr', 'o' => 'baz']), $this->_ldn->truncate(2));
	}
	
	public function testTruncateMore() : void
	{
		$this->assertEquals($this->_ldn, $this->_ldn->truncate(10));
	}
	
	public function testIteration() : void
	{
		foreach($this->_ldn as $key => $dn)
		{
			$this->assertIsInt($key);
			$this->assertInstanceOf(LdapDistinguishedPartInterface::class, $dn);
		}
	}
	
	public function testBuildDnPartArray() : void
	{
		$dn = new LdapDistinguishedName([
			new LdapDistinguishedPart('c', 'fr'),
			new LdapDistinguishedPart('o', 'baz'),
			new LdapDistinguishedPart('ou', 'bar'),
			new LdapDistinguishedPart('ou', 'foobar'),
			new LdapDistinguishedPart('cn', 'quux'),
		]);
		
		$this->assertEquals('cn=quux,ou=foobar,ou=bar,o=baz,c=fr', $dn->__toString());
	}
	
	public function testBuildDnSimpleArray() : void
	{
		$dn = new LdapDistinguishedName(['c' => 'fr', 'o' => 'baz', 'ou' => 'bar', 'cn' => 'foo']);
		
		$this->assertEquals('cn=foo,ou=bar,o=baz,c=fr', $dn->__toString());
	}
	
	public function testBuildDnSeparateArray() : void
	{
		$dn = new LdapDistinguishedName([['c', 'fr'], ['o', 'baz'], ['ou', 'bar'], ['ou', 'foobar'], ['cn', 'quux']]);
		
		$this->assertEquals('cn=quux,ou=foobar,ou=bar,o=baz,c=fr', $dn->__toString());
	}
	
	public function testBuildDnLinkedArray() : void
	{
		$dn = new LdapDistinguishedName([['c' => 'fr'], ['o' => 'baz'], ['ou' => 'bar'], ['ou' => 'foobar'], ['cn' => 'quux']]);
		
		$this->assertEquals('cn=quux,ou=foobar,ou=bar,o=baz,c=fr', $dn->__toString());
	}
	
	public function testGetFinalIdentifierFieldEmpty() : void
	{
		$this->assertEquals('', (new LdapDistinguishedName())->getFinalIdentifierField());
	}
	
	public function testGetFinalIdentifierValueEmpty() : void
	{
		$this->assertEquals('', (new LdapDistinguishedName())->getFinalIdentifierValue());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_ldn = new LdapDistinguishedName(['c' => 'fr', 'o' => 'baz', 'ou' => 'bar', 'cn' => 'foo']);
	}
	
}
