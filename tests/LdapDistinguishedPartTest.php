<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-dn-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ldap\LdapDistinguishedPart;
use PHPUnit\Framework\TestCase;

/**
 * LdapDistinguishedPartTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Ldap\LdapDistinguishedPart
 * 
 * @internal
 *
 * @small
 */
class LdapDistinguishedPartTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var LdapDistinguishedPart
	 */
	protected LdapDistinguishedPart $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('uuid=foo\\2abar\\7cbaz\\3dquux', $this->_object->__toString());
	}
	
	public function testGetName() : void
	{
		$this->assertEquals('uuid', $this->_object->getName());
	}
	
	public function testGetValue() : void
	{
		$this->assertEquals('foo*bar|baz=quux', $this->_object->getValue());
	}
	
	public function testGetStringRepresentation() : void
	{
		$this->assertEquals('uuid=foo\\2abar\\7cbaz\\3dquux', $this->_object->getStringRepresentation());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new LdapDistinguishedPart('uuid', 'foo*bar|baz=quux');
	}
	
}
