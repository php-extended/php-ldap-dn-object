<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-dn-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Ldap\LdapDistinguishedName;
use PhpExtended\Ldap\LdapDistinguishedNameParser;
use PhpExtended\Ldap\LdapDistinguishedPart;
use PHPUnit\Framework\TestCase;

/**
 * LdapDistinguishedNameParserTest class file.
 * 
 * @covers \PhpExtended\Ldap\LdapDistinguishedNameParser
 * @internal
 *
 * @small
 */
class LdapDistinguishedNameParserTest extends TestCase
{
	
	/**
	 * @var LdapDistinguishedNameParser
	 */
	protected LdapDistinguishedNameParser $_parser;
	
	public function testParseNull() : void
	{
		$this->assertEquals(new LdapDistinguishedName(), $this->_parser->parse(null));
	}
	
	public function testParseEmpty() : void
	{
		$this->assertEquals(new LdapDistinguishedName(), $this->_parser->parse(' '));
	}
	
	public function testParseNormal() : void
	{
		$ldapDn = 'cn=foobar,ou=foo,o=bar,c=fr';
		$parsed = $this->_parser->parse($ldapDn);
		$this->assertEquals(new LdapDistinguishedName([
			'c' => 'fr', 'o' => 'bar', 'ou' => 'foo', 'cn' => 'foobar',
		]), $parsed);
		$this->assertEquals($ldapDn, $parsed->getStringRepresentation());
	}
	
	public function testParseSpecial() : void
	{
		$ldapDn = 'cn=foo\\7cbar,c=fr';
		$parsed = $this->_parser->parse($ldapDn);
		$this->assertEquals(new LdapDistinguishedName(['c' => 'fr', 'cn' => 'foo|bar']), $parsed);
		$this->assertEquals($ldapDn, $parsed->getStringRepresentation());
	}
	
	public function testParseVerySpecial() : void
	{
		$ldapDn = 'cn=foo\\bar,c=fr';
		$parsed = $this->_parser->parse($ldapDn);
		$this->assertEquals(new LdapDistinguishedName(['c' => 'fr', 'cn' => 'foo\\bar']), $parsed);
	}
	
	public function testParseRedundantPartNames() : void
	{
		$ldapDn = 'cn=foo,ou=123,ou=456,ou=789,o=example,c=com';
		$parsed = $this->_parser->parse($ldapDn);
		$expected = new LdapDistinguishedName([
			new LdapDistinguishedPart('c', 'com'),
			new LdapDistinguishedPart('o', 'example'),
			new LdapDistinguishedPart('ou', '789'),
			new LdapDistinguishedPart('ou', '456'),
			new LdapDistinguishedPart('ou', '123'),
			new LdapDistinguishedPart('cn', 'foo'),
		]);
		$this->assertEquals($expected, $parsed);
		$this->assertEquals($ldapDn, $parsed->__toString());
	}
	
	protected function setUp() : void
	{
		$this->_parser = new LdapDistinguishedNameParser();
	}
	
}
