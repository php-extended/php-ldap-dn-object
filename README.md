# php-extended/php-ldap-dn-object

A library that implements the php-ldap-dn-interface library

![coverage](https://gitlab.com/php-extended/php-ldap-dn-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-ldap-dn-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-ldap-dn-object ^8`


## Basic Usage

This library creates dn with the following constructor :

```

use PhpExtended\Ldap\LdapDistinguishedName;

$ldn = new LdapDistinguishedName([
	'c' => 'fr',
	'o' => 'foo',
	'ou' => 'bar',
	'cn' => 'baz',
]);

// gets "cn=baz,ou=bar,o=foo,c=fr"

```

To parse the distinguished names, do :

```

use PhpExtended\Ldap\LdapDistinguishedNameParser;

$parser = new LdapDistinguishedNameParser();
$ldn = $parser->parse('cn=baz,ou=bar,o=foo,c=fr');

$ldn->getStringRepresentation(); // gets "cn=baz,ou=bar,o=foo,c=fr"

```


## License

MIT (See [license file](LICENSE)).
