<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-dn-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

/**
 * LdapDistinguishedPart class file.
 * 
 * This class is a simple implementation of the LdapDistinguishedPartInterface.
 * 
 * @author Anastaszor
 */
class LdapDistinguishedPart implements LdapDistinguishedPartInterface
{
	
	/**
	 * The name of the part.
	 * 
	 * @var string
	 */
	protected string $_name;
	
	/**
	 * The value of the part.
	 * 
	 * @var string
	 */
	protected string $_value;
	
	/**
	 * Builds a new LdapDistinguishedPart with its parts.
	 * 
	 * @param string $name
	 * @param string $value
	 */
	public function __construct(string $name, string $value)
	{
		$this->_name = $name;
		$this->_value = $value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return $this->getStringRepresentation();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapDistinguishedPartInterface::getName()
	 */
	public function getName() : string
	{
		return $this->_name;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapDistinguishedPartInterface::getValue()
	 */
	public function getValue() : string
	{
		return $this->_value;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapDistinguishedPartInterface::getStringRepresentation()
	 */
	public function getStringRepresentation() : string
	{
		return $this->ldapsecure($this->getName()).'='.$this->ldapsecure($this->getValue());
	}
	
	/**
	 * Replaces all special chacaters used in ldap queries by allowed characters
	 * in order to prevent ldap injection.
	 *
	 * @param string $string
	 * @return string
	 */
	public function ldapsecure(string $string) : string
	{
		return \strtr($string, [
			'\\0' => '\\00',
			'&' => '\\26',
			'(' => '\\28',
			')' => '\\29',
			'*' => '\\2a',
			'/' => '\\2f',
			'<' => '\\3c',
			'=' => '\\3d',
			'>' => '\\3e',
			'\\' => '\\5c',
			'|' => '\\7c',
			'~' => '\\7e',
		]);
	}
	
}
