<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-dn-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use PhpExtended\Parser\AbstractParser;

/**
 * LdapDistinguishedNameParser class file.
 * 
 * This class is a simple implementation of the LdapDistinguishedNameParserInterface.
 * 
 * @author Anastaszor
 * @extends \PhpExtended\Parser\AbstractParser<LdapDistinguishedNameInterface>
 */
class LdapDistinguishedNameParser extends AbstractParser implements LdapDistinguishedNameParserInterface
{
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Ldap\LdapDistinguishedNameParserInterface::parseString()
	 */
	public function parse(?string $data) : LdapDistinguishedNameInterface
	{
		if(null === $data || '' === $data)
		{
			return new LdapDistinguishedName();
		}
		
		$parts = \explode(',', $data);
		$nonempty = [];
		
		foreach($parts as $part)
		{
			$subparts = \explode('=', $part, 2);
			/** @phpstan-ignore-next-line */
			if(isset($subparts[0], $subparts[1]))
			{
				$fieldpart = \trim($subparts[0]);
				$datapart = \trim($subparts[1]);
				if(!empty($fieldpart) && !empty($datapart))
				{
					$nonempty[] = new LdapDistinguishedPart($this->ldapUnescape($fieldpart), $this->ldapUnescape($datapart));
				}
			}
		}
		
		return new LdapDistinguishedName(\array_reverse($nonempty));
	}
	
	/**
	 * Replaces all escape sequences in ldap values by their right characters.
	 *
	 * @param string $string
	 * @return string
	 */
	public function ldapUnescape(string $string) : string
	{
		$newString = $string;
		
		while(false !== \mb_strpos($newString, '\\'))
		{
			$unescaped = \str_replace([
				'\\00', '\\26', '\\28', '\\29', '\\2a', '\\2f', '\\3c', '\\3d', '\\3e', '\\5c', '\\7c', '\\7e',
			], [
				'\\0', '&', '(', ')', '*', '/', '<', '=', '>', '\\', '|', '~',
			], $newString);
			
			// prevent infinite loop with \ characters
			if($unescaped === $newString)
			{
				break;
			}
			$newString = $unescaped;
		}
		
		return $newString;
	}
	
}
